package com.apress.todo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TodoRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoRabbitmqApplication.class, args);
    }

}
