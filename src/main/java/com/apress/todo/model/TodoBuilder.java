package com.apress.todo.model;

public class TodoBuilder {
    private String name;
    private String description;

    public TodoBuilder setName(String name) {
this.name = name;
        return this;
    }
    public TodoBuilder setDesc(String desc) {
        this.description = desc;
        return this;
    }
     public ToDo build() {
        ToDo toDo = new ToDo();
        toDo.setName(this.name);
        toDo.setDescription(this.description);
        return toDo;
     }

}
