package com.apress.todo.service;

import com.apress.todo.model.TodoBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Log4j2
@Service
public class ScheduledBaySend {
    private final ToDoProducer toDoProducer;
    private final Environment environment;
    private static Integer k  = 0;
    @Scheduled(fixedRate = 5000L)
    public void send() {
        toDoProducer.sendTo(this.environment.getProperty("todo.amqp.key"), new TodoBuilder()
                .setName("mname " + k++)
                .setDesc("desc").build());
    }
}
