package com.apress.todo.service;

import com.apress.todo.model.ToDo;
import com.apress.todo.repository.ToDoRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ToDoConsumer {
private final ToDoRepository toDoRepository;

    public ToDoConsumer(ToDoRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }


    @RabbitListener(queues = "${todo.amqp.queue}")
    public void processTodo(ToDo message) {
        log.info("Consumer >> {}", message);
        this.toDoRepository.save(message);
        log.info("Save todo >> {}", message);
    }
    @RabbitListener(queues = "${todo.amqp.queue2}")
    public void processTodo2(ToDo message) {
        log.info("Consumer 2 >> {}", message);
        this.toDoRepository.save(message);
        log.info("Save todo 2 >> {}", message);
    }
}
