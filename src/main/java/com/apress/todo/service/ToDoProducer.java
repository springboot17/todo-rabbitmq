package com.apress.todo.service;

import com.apress.todo.model.ToDo;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@Log4j2
@AllArgsConstructor
public class ToDoProducer {
    private final RabbitTemplate rabbitTemplate;

    public void sendTo(String key, ToDo toDo)  {
        log.info("попытка отправить инфу");
        this.rabbitTemplate.convertAndSend(key, toDo);
    }
}
